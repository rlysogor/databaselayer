using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using EntityFrameworkLayers;
using DatabaseLayer.Repositories;

namespace WebApiSample.Controllers
{
    [Route("api/[controller]")]
    public class ValuesController : Controller
    {
        QzMainRepository dbContext = new QzMainRepository("User ID=postgres;Password=admin;Host=localhost;Port=5432;Database=projectQZ;Pooling=true;");
        // GET: api/values
        [HttpGet]
        public IEnumerable<string> Get()
        {
            var users = dbContext.QzUsers.GetAll();
            int userId = dbContext.QzUsers.Add("oleksii", "ruban", "oruban@ukr.net", "1323423423", "admin");
            var user13 = dbContext.QzUsers.GetEntityById(13);
            var questid = dbContext.Questionaries.Add(DateTime.Now, DateTime.Now.AddDays(2), true, 1, user13.id, EQzType.Rating);
            var participant = dbContext.Participants.Add(user13.id, questid, 3);
            var quest_for_user = dbContext.Questionaries.GetAllForUserId(user13.id);
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            try
            {

            }
            catch (Exception ex)
            {
                string msg = ex.Message;
            }
            return "value";
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
