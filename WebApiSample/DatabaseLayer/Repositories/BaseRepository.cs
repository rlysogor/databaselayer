﻿using Dapper;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DatabaseLayer.Repositories
{
    public abstract class BaseRepository<T>
    {
        protected string _connectionString;
        protected string _dbTableName;

        public BaseRepository(string connString, string dbTableName)
        {
            _connectionString = connString;
            _dbTableName = dbTableName;

        }

        public virtual List<T> GetAll()
        {
            try
            {
                using (NpgsqlConnection conn = new NpgsqlConnection(_connectionString))
                {
                    conn.Open();
                    string sqlCommand = String.Format("SELECT * FROM {0}", _dbTableName);
                    var queryResult = conn.Query<T>(sqlCommand, null).ToList<T>();
                    conn.Close();
                    return queryResult;
                }
            }
            catch
            {
                //bla
            }

            return null;
        }

        public virtual T GetEntityById(int Id)
        {
            try
            {
                using (NpgsqlConnection conn = new NpgsqlConnection(_connectionString))
                {
                    conn.Open();
                    string sqlCommand = String.Format("SELECT * FROM {0} where id = @Id", _dbTableName);
                    var qzUser = conn.Query<T>(sqlCommand, new { Id }).FirstOrDefault<T>();
                    conn.Close();
                    if (qzUser != null) return qzUser;
                }
            }
            catch
            {
                //bla
            }

            return default(T);
        }
    }
}
