﻿using Dapper;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DatabaseLayer.Repositories
{
    public class DecisionsRepository : BaseRepository<DbDecisions>
    {
        public DecisionsRepository(string connectionString) : base(connectionString, "decisions") { }

        public int Add(string DecisionText)
        {
            int insertedId = -1;
            try
            {
                using (NpgsqlConnection conn = new NpgsqlConnection(_connectionString))
                {
                    conn.Open();
                    string sqlCommand = String.Format(@"INSERT INTO decisions(decision_text) VALUES (@DecisionText) RETURNING id",
                                                      _dbTableName);

                    var queryResult = conn.Query<int>(sqlCommand,
                        new
                        {
                            DecisionText
                        })
                        .FirstOrDefault<int>();
                    conn.Close();

                    if (queryResult < 1) throw new InvalidOperationException(String.Format("DecisionText ({0}) was not added in the database", DecisionText));
                    insertedId = queryResult;
                }
            }
            catch (Exception ex)
            {
                //bla
            }

            return insertedId;
        }
    }

    public class DbDecisions
    {
        public int id;
        public string decision_text;
    }
}
