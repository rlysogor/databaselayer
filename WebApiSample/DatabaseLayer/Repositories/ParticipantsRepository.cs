﻿using Dapper;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DatabaseLayer.Repositories
{
    public class ParticipantsRepository : BaseRepository<DbParticipant>
    {
        public ParticipantsRepository(string connectionString) : base(connectionString, "participants") { }

        public int Add(int UserId, int QuestId, int AccessType)
        {
            int insertedId = -1;
            try
            {
                using (NpgsqlConnection conn = new NpgsqlConnection(_connectionString))
                {
                    conn.Open();
                    string sqlCommand = String.Format(@"INSERT INTO participants(user_id, quest_id, access_type, voted)
                                                        VALUES(@UserId, @QuestId, @AccessType, @voted) RETURNING id",
                                                      _dbTableName);

                    var queryResult = conn.Query<int>(sqlCommand,
                        new
                        {
                            UserId,
                            QuestId,
                            AccessType,
                            voted = false,
                        })
                        .FirstOrDefault<int>();
                    conn.Close();

                    if (queryResult < 1) throw new InvalidOperationException(String.Format("Participant user({0}) for Questionnaire ({1}) was not added in the database", UserId, QuestId));
                    insertedId = queryResult;
                }
            }
            catch (Exception ex)
            {
                //bla
            }

            return insertedId;
        }

        public List<DbParticipant> GetAllForQuestionnaire(int QuestId)
        {
            try
            {
                using (NpgsqlConnection conn = new NpgsqlConnection(_connectionString))
                {
                    conn.Open();
                    string sqlCommand = String.Format("SELECT * FROM {0} where quest_id = @QuestId", _dbTableName);
                    var queryResult = conn.Query<DbParticipant>(sqlCommand, null).ToList<DbParticipant>();
                    conn.Close();
                    return queryResult;
                }
            }
            catch
            {
                //bla
            }

            return null;
        }
    }

    public class DbParticipant
    {
        public int id;
        public int user_id;
        public int quest_id;
        bool voted;
    }
}
