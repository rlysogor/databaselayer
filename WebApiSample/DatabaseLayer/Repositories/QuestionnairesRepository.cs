﻿using Dapper;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DatabaseLayer.Repositories
{
    public class QuestionnairesRepository : BaseRepository<DbQuestionnaire>
    {
        public QuestionnairesRepository(string connectionString) : base(connectionString, "questionnaires") { }

        public int Add(DateTime StartDate, DateTime? EndDate, bool WaitAllParticipants, int Orderid, int UserId, EQzType QuestType)
        {
            int insertedId = -1;
            try
            {
                using (NpgsqlConnection conn = new NpgsqlConnection(_connectionString))
                {
                    conn.Open();
                    string sqlCommand = String.Format(@"INSERT INTO {0} (added, modified, status, start_date, end_date,
                                                        wait_all_participants, orderid, qzuser_id, quest_type_id)
                                                        VALUES (@added, @modified, @status, @StartDate, @EndDate, @WaitAllParticipants, @Orderid, @UserId, @QuestType) RETURNING id",
                                                      _dbTableName);

                    var queryResult = conn.Query<int>(sqlCommand,
                        new
                        {
                            added = DateTime.Now,
                            modified = DateTime.Now,
                            status = EQzStatus.Started,
                            StartDate,
                            EndDate = EndDate.HasValue ? EndDate.Value : DateTime.MaxValue,
                            Orderid,
                            UserId,
                            WaitAllParticipants,
                            QuestType
                        })
                        .FirstOrDefault<int>();
                    conn.Close();

                    if (queryResult < 1) throw new InvalidOperationException(String.Format("Questionaire for user({0}) was not added in the database", UserId));
                    insertedId = queryResult;
                }
            }
            catch(Exception ex)
            {
                //bla
            }

            return insertedId;
        }

        public override List<DbQuestionnaire> GetAll()
        {
            return base.GetAll();
        }

        public List<DbQuestionnaire> GetAllForUserId(int UserId)
        {
            try
            {
                using (NpgsqlConnection conn = new NpgsqlConnection(_connectionString))
                {
                    conn.Open();
                    string sqlCommand = String.Format("SELECT * FROM {0} where qzuser_id = @UserId", _dbTableName);
                    var queryResult = conn.Query<DbQuestionnaire>(sqlCommand, new {UserId }).ToList<DbQuestionnaire>();
                    return queryResult;
                    conn.Close();
                }
            }
            catch
            {
                //bla
            }

            return null;
        }

        public override DbQuestionnaire GetEntityById(int Id)
        {
            return base.GetEntityById(Id);
        }
    }

    public class DbQuestionnaire
    {
        public int id;
        public DateTime added;
        public DateTime modified;
        public EQzStatus status;
        public DateTime start_date;
        public DateTime end_date;
        public bool wait_all_participants;
        public int orderid;
        public int qzuser_id;
        public EQzType quest_type_id;
        public EQzAccessType visibility_level;
    }

    public enum EQzType
    {
        Default = 1,
        Rating = 2
    }

    public enum EQzStatus
    {
        Creating = 0,
        Started = 1,
        Finished = 2,
        Closed = 3
    }

    public enum EQzAccessType
    {
        Public = 0,
        Private = 1,
        Shared = 2
    }
}