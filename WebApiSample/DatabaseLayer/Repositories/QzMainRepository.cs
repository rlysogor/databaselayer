﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DatabaseLayer.Repositories
{
    public class QzMainRepository
    {
        private string _connectionString = "User ID=postgres;Password=admin;Host=localhost;Port=5432;Database=projectQZ;Pooling=true;";

        public QzMainRepository(string connString)
        {
            _connectionString = connString;
            QzUsers = new QzUsersRepository(_connectionString);
            Questionaries = new QuestionnairesRepository(_connectionString);
            Participants = new ParticipantsRepository(_connectionString);
        }

        public QzUsersRepository QzUsers { get; private set; }

        public QuestionnairesRepository Questionaries { get; private set; }

        public ParticipantsRepository Participants { get; private set; }
    }
}
