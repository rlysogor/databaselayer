﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Npgsql;
using Dapper;

namespace DatabaseLayer.Repositories
{
    public class QzUsersRepository : BaseRepository<DbQzUser>
    {
        public QzUsersRepository(string connectionString) : base(connectionString, "qzusers") { }

        public int Add(string FirstName, string LastName, string Email, string PhoneNumber, string Type)
        {
            int insertedId = -1;
            try
            {
                using (NpgsqlConnection conn = new NpgsqlConnection(_connectionString))
                {
                    conn.Open();
                    string sqlCommand = String.Format(@"INSERT INTO {0} (added, modified, first_name, last_name, email, phone_number, user_type)
                                                      VALUES(@added, @modified, @FirstName, @LastName, @Email, @PhoneNumber, @Type) RETURNING id",
                                                      _dbTableName);

                    var queryResult = conn.Query<int>(sqlCommand,
                        new
                        {
                            added = DateTime.Now,
                            modified = DateTime.Now,
                            FirstName,
                            LastName,
                            Email,
                            PhoneNumber,
                            Type
                        })
                        .FirstOrDefault<int>();
                    conn.Close();

                    if (queryResult < 1) throw new InvalidOperationException(String.Format("User : ({0} {1})  was not added in the database", FirstName, LastName));
                    insertedId = queryResult;
                }
            }
            catch
            {
                //bla
            }

            return insertedId;
        }

        public override List<DbQzUser> GetAll()
        {
            return base.GetAll();
        }
        
        public override DbQzUser GetEntityById(int Id)
        {
            return base.GetEntityById(Id);
        }
    }

    public class DbQzUser
    {
        public int id;
        public DateTime added;
        public DateTime modified;
        public string first_name;
        public string last_name;
        public string email;
        public string phone_number;
        public string user_type;

    }
}
